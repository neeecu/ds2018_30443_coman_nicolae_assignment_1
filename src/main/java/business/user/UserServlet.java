package business.user;

import business.HtmlStrings;
import business.servletUtil.ServletUtil;
import dataAccess.dao.CityDao;
import dataAccess.dao.FlightDao;
import dataAccess.entity.City;
import dataAccess.entity.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UserServlet extends HttpServlet {

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session=req.getSession(false);
        if( session!=null && session.getAttribute("role").equals("CUSTOMER")) {
            List<Flight> flights = new ArrayList<>();
            FlightDao flightDao = new FlightDao();
            CityDao cityDao = new CityDao();
            flights = flightDao.findAll();
            PrintWriter out = resp.getWriter();
            String currentUser = req.getParameter("userName");
            /*html doc*/
            out.print(HtmlStrings.DOCTYPE);
            /*header*/
            out.print(HtmlStrings.TABLE_PAGE_HEADER);
            /*body*/
            out.print(HtmlStrings.makeTableHeader(Arrays.asList("Departure City","Arrival City","Airplane Type")));
            for(int i=0; i<flights.size();i++){
                 out.print(HtmlStrings.makeTableEntry(Arrays.asList(flights.get(i).getDepartureCity().getName(),
                        flights.get(i).getArrivalCity().getName(),flights.get(i).getAirplaneType().toString())));

            }
            out.print(HtmlStrings.TABLE_END_TAG);
            /*get local time for city*/
            out.print(HtmlStrings.startForm("city/search","get"));
            out.print(HtmlStrings.LIST_START_TAG);
            out.print(HtmlStrings.makeDropDownList(cityDao.findAll()
                .stream()
                .map(City::getName)
                .collect(Collectors.toList()),"webServiceCity"));
            out.print(HtmlStrings.makeButtonInForm("", "Get City Local Time"));
            out.print(HtmlStrings.END_HTML_TAG);
            out.print(HtmlStrings.FORM_END_TAG);

        }
        else{
            ServletUtil.refuseAccess(req,resp,"http://localhost:8080/admin","AdminPage");
        }
    }

}
