package business.servletUtil;

import business.HtmlStrings;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ServletUtil {

    public static void refuseAccess(HttpServletRequest req, HttpServletResponse resp, String redirectLin,
                             String buttonText) throws IOException{

        resp.setContentType(HtmlStrings.HTML);
        PrintWriter out = resp.getWriter();
        /*html doc*/
        out.print(HtmlStrings.DOCTYPE);
        /*header*/
        out.print(HtmlStrings.makeHead("Error Accessing Page"));
        /*body*/
        out.print(HtmlStrings.BODY_START_TAG);
        out.print(HtmlStrings.makeParagraph("Please login first. " +
                "If you are already logged in, you do not have access to this page"));
        out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/login","Login"));
        out.print(HtmlStrings.HTML_NEW_LINE);
        out.print(HtmlStrings.makeButtonWithLink(redirectLin,buttonText));
        out.print(HtmlStrings.BODY_END_TAG);
        out.print(HtmlStrings.END_HTML_TAG);

    }
}
