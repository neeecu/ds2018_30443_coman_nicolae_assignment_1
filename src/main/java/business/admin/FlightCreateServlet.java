package business.admin;

import business.HtmlStrings;
import business.servletUtil.ServletUtil;
import dataAccess.dao.CityDao;
import dataAccess.dao.FlightDao;
import dataAccess.entity.AirplaneType;
import dataAccess.entity.City;
import dataAccess.entity.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

public class FlightCreateServlet extends HttpServlet {

     public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         HttpSession session=req.getSession(false);
         if( session!=null && session.getAttribute("role").equals("ADMIN")) {
             resp.setContentType(HtmlStrings.HTML);
             PrintWriter out = resp.getWriter();
             String departureHour = req.getParameter("departureHour");
             String arrivalHour = req.getParameter("arrivalHour");
             String departureCity = req.getParameter("departureCity");
             String arrivalCity = req.getParameter("arrivalCity");
             String airplaneType = req.getParameter("airplaneType");
             if (departureHour.isEmpty() || arrivalCity.isEmpty() || departureCity.isEmpty()
                     || arrivalHour.isEmpty() || airplaneType.isEmpty()) {
                 out.print(HtmlStrings.makeHead("Error"));
                 out.print(HtmlStrings.makeParagraph("Please go back and complete all fields"));
                 out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin", "Back"));
                 resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
             } else {
                 resp.setStatus(HttpServletResponse.SC_OK);
                 LocalDateTime depHour = LocalDateTime.parse(departureHour);
                 LocalDateTime arrHour = LocalDateTime.parse(arrivalHour);
                 CityDao cityDao = new CityDao();
                 FlightDao flightDao = new FlightDao();
                 Flight flight = new Flight(Enum.valueOf(AirplaneType.class, airplaneType)
                         ,depHour,arrHour);
                 if (cityDao.findCityByNAme(departureCity) == null) {
                     cityDao.save(City.builder()
                             .name(departureCity)
                             .latitude(0.0)
                             .longitude(0.0)
                             .cityFlights(new ArrayList<>())
                             .build());

                 } /*else city exists in db*/ else{
                     flight.setDepartureCity(cityDao.findCityByNAme(departureCity));
                 }

                 if (cityDao.findCityByNAme(arrivalCity) == null) {
                     cityDao.save(City.builder()
                             .name(arrivalCity)
                             .latitude(0.0)
                             .longitude(0.0)
                             .cityFlights(new ArrayList<>())
                             .build());
                 } else{
                     flight.setArrivalCity(cityDao.findCityByNAme(arrivalCity));
                 }

                 int flightId = flightDao.save(flight);
                 out.print(HtmlStrings.makeParagraph("Successfully created flight with id = " + flightId));
                 out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin", "Back"));
             }
         }
         else{
             ServletUtil.refuseAccess(req,resp,"http://localhost:8080/user","UserPage");
         }
    }
}
