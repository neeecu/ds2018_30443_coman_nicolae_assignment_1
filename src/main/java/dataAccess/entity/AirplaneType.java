package dataAccess.entity;

public enum AirplaneType {
    AIRLINER,
    BOEING,
    BOMBARDIER
}

