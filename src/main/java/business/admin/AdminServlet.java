package business.admin;

import business.HtmlStrings;
import business.servletUtil.ServletUtil;
import com.google.code.geocoder.model.GeocoderResult;
import dataAccess.dao.FlightDao;
import dataAccess.entity.AirplaneType;
import dataAccess.entity.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class AdminServlet extends HttpServlet {

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session=req.getSession(false);
        if( session!=null && session.getAttribute("role").equals("ADMIN")) {
            resp.setContentType(HtmlStrings.HTML);
            PrintWriter out = resp.getWriter();
            String currentAdmin = req.getParameter("userName");

            /*html doc*/
            out.print(HtmlStrings.DOCTYPE);
            /*header*/
            out.print(HtmlStrings.makeHead("Admin -" + currentAdmin));
            /*body*/
            out.print(HtmlStrings.BODY_START_TAG);
            out.print(HtmlStrings.makeH("AdminOperations", 3));
            out.print(HtmlStrings.HTML_NEW_LINE);
            out.print(HtmlStrings.makeH("Find a flight", 3));
            /*list without dots*/
            out.print(HtmlStrings.LIST_START_TAG);
            /*form find flight*/
            out.print(HtmlStrings.startForm("admin", "post"));
            out.print(HtmlStrings.makeTextInputEntry("flightId", "flightId"));
            out.print(HtmlStrings.makeButtonInForm("", "GetFlight"));
            out.print(HtmlStrings.FORM_END_TAG);

            out.print(HtmlStrings.HTML_NEW_LINE);

            /*form delete flight*/
            out.print(HtmlStrings.makeH("Delete Flight", 3));
            out.print(HtmlStrings.startForm("admin/delete", "post"));
            out.print(HtmlStrings.makeTextInputEntry("flightId", "flightId"));
            out.print(HtmlStrings.makeButtonInForm("", "deleteFlight"));
            out.print(HtmlStrings.FORM_END_TAG);

            out.print(HtmlStrings.HTML_NEW_LINE);

            /*form update flight*/
            out.print(HtmlStrings.makeH("Update Flight", 3));
            out.print(HtmlStrings.startForm("admin/update", "post"));
            out.print(HtmlStrings.makeTextInputEntry("flightId", "flightId"));
            out.print(HtmlStrings.makeTextInputEntry("departureCity", "departureCity"));
            out.print(HtmlStrings.makeTextInputEntry("arrivalCity", "arrivalCity"));
            //add empty string in case we don't want to update the role
            out.print(HtmlStrings.makeDropDownList(Arrays.asList(AirplaneType.AIRLINER.toString(),
                    AirplaneType.BOEING.toString(), AirplaneType.BOMBARDIER.toString(), ""), "airplaneType"));

            out.print(HtmlStrings.makeButtonInForm("", "updateFlight"));
            out.print(HtmlStrings.FORM_END_TAG);

            /*form create flight*/
            out.print(HtmlStrings.makeH("Add New Flight", 3));
            out.print(HtmlStrings.startForm("admin/create", "post"));
            out.print(HtmlStrings.makeTextInputEntry("departureCity", "departureCity"));
            out.print(HtmlStrings.makeTextInputEntry("arrivalCity", "arrivalCity"));
            out.print(HtmlStrings.makeLabel("DepartureHour: "));
            out.print(HtmlStrings.makeTimeInputEntry("departureHour"));
            out.print(HtmlStrings.makeLabel("ArrivalHour: "));
            out.print(HtmlStrings.makeTimeInputEntry("arrivalHour"));
            out.print(HtmlStrings.makeDropDownList(Arrays.asList(AirplaneType.AIRLINER.toString(),
                    AirplaneType.BOEING.toString(), AirplaneType.BOMBARDIER.toString(), ""), "airplaneType"));
            out.print(HtmlStrings.makeButtonInForm("", "addFlight"));
            out.print(HtmlStrings.FORM_END_TAG);

            out.print(HtmlStrings.LIST_END_TAG);

            out.print(HtmlStrings.BODY_END_TAG);
            out.print(HtmlStrings.END_HTML_TAG);
        }
        else{
            ServletUtil.refuseAccess(req,resp,"http://localhost:8080/user","UserPage");
        }
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType(HtmlStrings.HTML);
        PrintWriter out = resp.getWriter();
        String flightIdString = req.getParameter("flightId");
        if(flightIdString.isEmpty()){
            out.print(HtmlStrings.makeHead("Error"));
            out.print(HtmlStrings.makeParagraph("Please go back and enter an id"));
            out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin","Back"));
            resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
        }
        else if(!flightIdString.chars().allMatch( Character::isDigit ))
        {
            out.print(HtmlStrings.makeHead("Error"));
            out.print(HtmlStrings.makeParagraph("Please go back and enter a valid id." +
                    "\n It must be a positive number"));
            out.print(HtmlStrings.HTML_NEW_LINE);
            out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin","Back"));
            resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
        }
        else {

            FlightDao flightDao = new FlightDao();
            int flightId = Integer.valueOf(flightIdString);
            Flight flight = flightDao.findFlightById(flightId);
                 if(flight == null) {
                     resp.setStatus(HttpServletResponse.SC_FOUND);
                     out.print(HtmlStrings.makeHead("Error"));
                     out.print(HtmlStrings.makeParagraph("There is no flight for the givenId"));
                     out.print(HtmlStrings.HTML_NEW_LINE);
                     out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin","Back"));

                 } else{
                     resp.setStatus(HttpServletResponse.SC_OK);
                     out.print(HtmlStrings.LIST_START_TAG);
                     out.print(HtmlStrings.makeLabel("AirplaneType: "));
                     out.print(flight.getAirplaneType());
                     out.print(HtmlStrings.HTML_NEW_LINE);
                     out.print(HtmlStrings.makeLabel("Departure: "));
                     out.print(flight.getDepartureCity().getName()+", "+flight.getDepartureHour());
                     out.print(HtmlStrings.HTML_NEW_LINE);
                     out.print(HtmlStrings.makeLabel("Arrival: "));
                     out.print(flight.getArrivalCity().getName()+", "+flight.getArrivingHour());
                     out.print(HtmlStrings.HTML_NEW_LINE);
                     out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin","Back"));

                 }
            }
    }

}
