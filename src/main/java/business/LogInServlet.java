package business;

import dataAccess.dao.UserDao;
import dataAccess.entity.Role;
import dataAccess.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


public class LogInServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher =request.getRequestDispatcher("login.html");
        requestDispatcher.forward(request,response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");

        String username = request.getParameter("userName");
        String password = request.getParameter("pass");

        UserDao dao = new UserDao();

        User toFind = dao.findByName(username);
        if(toFind != null && password.equals(toFind.getPassword())){
            HttpSession session=request.getSession();
            response.setStatus(HttpServletResponse.SC_OK);
            session.setAttribute("role",toFind.getUserRole().toString());
            if(toFind.getUserRole() == Role.ADMIN) {
                response.sendRedirect("/admin?userName=" + username);
            }
            else if (toFind.getUserRole() == Role.CUSTOMER) {
                response.sendRedirect("/user?userName=" + username);
            }
            else{
                response.sendRedirect("/login");
            }
        }
        else{
             response.setStatus(HttpServletResponse.SC_FOUND);
             response.sendRedirect("/login");
        }
    }
}
