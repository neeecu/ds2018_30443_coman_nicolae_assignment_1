package business.admin;

import business.HtmlStrings;
import business.servletUtil.ServletUtil;
import dataAccess.dao.CityDao;
import dataAccess.dao.FlightDao;
import dataAccess.entity.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FlightUpdateServlet extends HttpServlet {


     public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         HttpSession session=req.getSession(false);
         if( session!=null && session.getAttribute("role").equals("ADMIN")) {
             resp.setContentType(HtmlStrings.HTML);
             PrintWriter out = resp.getWriter();
             String flightIdString = req.getParameter("flightId");
             String departureCity = req.getParameter("departureCity");
             String arrivalCity = req.getParameter("arrivalCity");
             String airplaneType = req.getParameter("airplaneType");
             if (flightIdString.isEmpty()) {
                 out.print(HtmlStrings.makeHead("Error"));
                 out.print(HtmlStrings.makeParagraph("Please go back and enter an id"));
                 out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin", "Back"));
                 resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
             } else if (!flightIdString.chars().allMatch(Character::isDigit)) {
                 out.print(HtmlStrings.makeHead("Error"));
                 out.print(HtmlStrings.makeParagraph("Please go back and enter a valid id." +
                         "\nIt has to be a positive number"));
                 out.print(HtmlStrings.HTML_NEW_LINE);
                 out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin", "Back"));
                 resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
             } else {
                 FlightDao flightDao = new FlightDao();
                 CityDao cityDao = new CityDao();
                 int flightId = Integer.valueOf(flightIdString);
                 Flight flight = flightDao.findFlightById(flightId);
                 if (flight == null) {
                     resp.setStatus(HttpServletResponse.SC_FOUND);
                     out.print(HtmlStrings.makeHead("Error"));
                     out.print(HtmlStrings.makeParagraph("There is no user with the givenId "));
                     out.print(HtmlStrings.HTML_NEW_LINE);
                     out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin", "Back"));


                 } else {
                     /*if cities not in db*/
                     if (cityDao.findCityByNAme(departureCity) == null && !departureCity.isEmpty()) {
                         City depCity = City.builder().name(departureCity)
                                 .latitude(0.0)
                                 .longitude(0.0)
                                 .cityFlights(new ArrayList<>())
                                 .build();
                         cityDao.save(depCity);
                         flight.setDepartureCity(depCity);
                     }
                     if (cityDao.findCityByNAme(arrivalCity) == null && !arrivalCity.isEmpty()) {
                         City arrCity = City.builder().name(arrivalCity)
                                 .latitude(0.0)
                                 .longitude(0.0)
                                 .cityFlights(new ArrayList<>())
                                 .build();
                         cityDao.save(arrCity);
                         flight.setArrivalCity(arrCity);
                     }
                     /*if cities in db*/
                     if(cityDao.findCityByNAme(departureCity)!=null){
                         flight.setDepartureCity(cityDao.findCityByNAme(departureCity));
                     }
                     if(cityDao.findCityByNAme(arrivalCity)!=null){
                         flight.setArrivalCity(cityDao.findCityByNAme(arrivalCity));
                     }

                     resp.setStatus(HttpServletResponse.SC_OK);
                     if (!airplaneType.isEmpty()) {
                         flight.setAirplaneType(AirplaneType.valueOf(airplaneType));
                     }
                     flightDao.update(flight);
                     out.print(HtmlStrings.makeParagraph("Successfully updated user with id =" + flightIdString));
                     out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin", "Back"));
                 }
             }
         } else {
             ServletUtil.refuseAccess(req,resp,"http://localhost:8080/user","UserPage");
         }
    }
}
