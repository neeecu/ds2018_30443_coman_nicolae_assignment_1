import dataAccess.dao.FlightDao;
import dataAccess.dao.UserDao;
import dataAccess.entity.Flight;
import dataAccess.entity.Role;
import dataAccess.entity.User;
import dataAccess.util.HibernateUtil;

import java.util.List;

public class App {

    public static void main(String[] args) {
        FlightDao flightDao = new FlightDao();
        List<Flight> flightList  = flightDao.findFlightsByArrivalCity(2);
        flightList.stream().forEach( l -> System.out.println(l.getDepartureCity().getName() +"\n"
        +l.getArrivalCity().getName()));

       User user = User.builder().userRole(Role.ADMIN)
               .username("admin")
               .password("admin")
               .build();

       HibernateUtil.shutdown();
    }
}
