package dataAccess.entity;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "flights")
public class Flight {

    @Id
    @GeneratedValue
    private int flightId; //flight number

    @Enumerated(EnumType.STRING)
    private AirplaneType airplaneType;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,CascadeType.MERGE,CascadeType.DETACH} ,fetch = FetchType.EAGER)
    @JoinTable(
            name = "Flight_City",
            joinColumns = { @JoinColumn(name = "fk_flight")},
            inverseJoinColumns = {@JoinColumn(name ="fk_city")}
    )
    //list intended to have only two elements,
    //flightCities(0) - departure city
    //flightCities(1) - arrival city
    private List<City> flightCities ;

    private LocalDateTime departureHour;
    private LocalDateTime arrivingHour;

    public Flight(AirplaneType airplaneType,LocalDateTime departureHour, LocalDateTime arrivingHour) {
        this.airplaneType = airplaneType;
        this.departureHour = departureHour;
        this.arrivingHour = arrivingHour;
        this.flightCities= Arrays.asList(new City[2]);
    }

    public void setDepartureCity(City city){
        this.flightCities.set(0,city);
        city.getCityFlights().add(this);

    }

    public void setArrivalCity(City city){
        this.flightCities.set(1,city);
        city.getCityFlights().add(this);

    }

    public City getDepartureCity(){
        return this.flightCities.get(0);
    }

    public City getArrivalCity(){
        return this.flightCities.get(1);
    }

}
