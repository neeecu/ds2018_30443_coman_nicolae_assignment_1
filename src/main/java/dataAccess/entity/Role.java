package dataAccess.entity;

public enum Role {
    ADMIN,
    CUSTOMER,
    NOT_DEFINED
}
