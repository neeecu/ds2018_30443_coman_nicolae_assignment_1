package business.user;

import business.HtmlStrings;
import business.servletUtil.ServletUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dataAccess.dao.CityDao;
import dataAccess.entity.City;
import dataAccess.util.GeoResponsePojo;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GetHourServlet extends HttpServlet {


    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);
        if (session != null && session.getAttribute("role").equals("CUSTOMER")) {
            resp.setContentType(HtmlStrings.HTML);
            PrintWriter out = resp.getWriter();
            String webServiceCity = req.getParameter("webServiceCity");
            out.print(webServiceCity);
            String baseUrl = "http://api.geonames.org/timezoneJSON?lat=latToReplace&lng=lngToReplace&username=" +
                    "nicolae.coman";
            CityDao cityDao = new CityDao();
            City cityObject = cityDao.findCityByNAme(webServiceCity);
            baseUrl = baseUrl.replace("latToReplace", String.valueOf(cityObject.getLatitude()));
            baseUrl = baseUrl.replace("lngToReplace", String.valueOf(cityObject.getLongitude()));

            StringBuilder strBuf = new StringBuilder();
            HttpURLConnection conn = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(baseUrl);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("HTTP GET Request Failed with Error code : "
                            + conn.getResponseCode());
                }
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
                String output = null;
                while ((output = reader.readLine()) != null)
                    strBuf.append(output);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (conn != null) {
                    conn.disconnect();
                }
            }
            Gson gson = new GsonBuilder().create();
            GeoResponsePojo pojo = gson.fromJson(strBuf.toString(), GeoResponsePojo.class);
            out.print(HtmlStrings.HTML_NEW_LINE);
            out.print("Local hour in city "+webServiceCity+" is "+pojo.getTime());
            out.print(HtmlStrings.HTML_NEW_LINE);
            out.print(HtmlStrings.makeButtonWithLink("/user","Back"));
        }
        else{
            ServletUtil.refuseAccess(req,resp,"http://localhost:8080/admin","AdminPage");

        }
    }
}
