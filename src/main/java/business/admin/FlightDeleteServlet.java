package business.admin;

import business.HtmlStrings;
import business.servletUtil.ServletUtil;
import dataAccess.dao.FlightDao;
import dataAccess.entity.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class FlightDeleteServlet extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session=req.getSession(false);
        if( session!=null && session.getAttribute("role").equals("ADMIN")) {
            resp.setContentType(HtmlStrings.HTML);
            PrintWriter out = resp.getWriter();
            String flightIdString = req.getParameter("flightId");
            if (flightIdString.isEmpty()) {
                out.print(HtmlStrings.makeHead("Error"));
                out.print(HtmlStrings.makeParagraph("Please go back and enter an id"));
                out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin", "Back"));
                resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            } else if (!flightIdString.chars().allMatch(Character::isDigit)) {
                out.print(HtmlStrings.makeHead("Error"));
                out.print(HtmlStrings.makeParagraph("Please go back and enter a valid id." +
                        "\n It has to be a positive number"));
                out.print(HtmlStrings.HTML_NEW_LINE);
                out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin", "Back"));
                resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            } else {
                FlightDao flightDao = new FlightDao();
                int flightID = Integer.valueOf(flightIdString);
                Flight flight = flightDao.deleteFlight(flightID);

                if (flight == null) {
                    resp.setStatus(HttpServletResponse.SC_FOUND);
                    out.print(HtmlStrings.makeHead("Error"));
                    out.print(HtmlStrings.makeParagraph("There is no flight for the givenId "));
                    out.print(HtmlStrings.HTML_NEW_LINE);
                    out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin", "Back"));
                } else {
                    resp.setStatus(HttpServletResponse.SC_OK);

                    out.print(HtmlStrings.makeParagraph("Successfully deleted flight with id =" + flightIdString));

                    out.print(HtmlStrings.makeButtonWithLink("http://localhost:8080/admin", "Back"));
                }
            }
        } else {
            ServletUtil.refuseAccess(req,resp,"http://localhost:8080/user","UserPage");
        }
    }
}
