package dataAccess.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor

@Entity
@Table
public class City {

    @Id
    @GeneratedValue
    private int cityId;

    private String name;
    private double longitude;
    private double latitude;
    @ManyToMany(mappedBy = "flightCities", fetch = FetchType.EAGER)
    private List<Flight> cityFlights =new ArrayList<Flight>();

    @Builder
    public City(String name, double longitude, double latitude, List<Flight> cityFlights) {
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.cityFlights = cityFlights;
    }
}

