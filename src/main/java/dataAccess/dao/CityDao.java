package dataAccess.dao;

import dataAccess.entity.City;
import dataAccess.entity.Flight;
import dataAccess.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class CityDao {

    private Session session;

    public City findCityByNAme(String cityName){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List<City> cities = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE name = :name");
            query.setParameter("name", cityName);
            cities = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }

        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;

    }

    public int save(City city) {
        int cityId = -1;
        session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction  = null;
        try {
            transaction = session.beginTransaction();

            cityId = (Integer) session.save(city);
            city.setCityId(cityId);
            transaction.commit();
        }catch (HibernateException e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        finally{
            session.close();
        }

        return cityId;

    }

    public List<City> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List<City> cities = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM City");
            cities = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return cities;
    }
}
