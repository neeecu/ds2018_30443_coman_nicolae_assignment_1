package dataAccess.dao;

import dataAccess.entity.Flight;
import dataAccess.entity.User;
import dataAccess.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;
import java.util.stream.Collectors;

public class FlightDao {
    private Session session;

    public int save(Flight flight) {
        int flightId = -1;
        session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            flightId = (Integer) session.save(flight);
            flight.setFlightId(flightId);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return flightId;
    }

    public Flight deleteFlight(int flightId) {
        Flight flight = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            flight = findFlightById(flightId);
            if (flight != null) {
                session.delete(flight);
                transaction.commit();
            }
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
                //throw exception to notify user that user to delete doesnt exist
                throw e;
            }

        } finally {
            session.close();
        }
        return flight;
    }

    @SuppressWarnings("unchecked")
    public Flight findFlightById(int flightId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List<Flight> flights = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", flightId);
            flights = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }

        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    @SuppressWarnings("unchekced")
    public List<Flight> findFlightsByArrivalCity(int cityId)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List<Flight> flights = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery
                    ("select f from Flight f join f.flightCities c where c.cityId =:fk_city");
            query.setParameter("fk_city", cityId);
            flights = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }

        } finally {
            session.close();
        }
        //return just thw flights where cityId corresponds to arrival city
        List<Flight> wantedFlights  = flights.stream()
            .filter(l->l.getArrivalCity().getCityId()==cityId)
            .collect(Collectors.toList());
        return wantedFlights;
    }

    @SuppressWarnings("unchekced")
    public List<Flight> findFlightsByDepartureCity(int cityId)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List<Flight> flights = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery
                    ("select f from Flight f join f.flightCities c where c.cityId =:city_id");
            query.setParameter("city_id", cityId);
            flights = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }

        } finally {
            session.close();
        }
        //return just thw flights where cityId corresponds to arrival city
        List<Flight> wantedFlights  = flights.stream()
                .filter(l->l.getDepartureCity().getCityId()==cityId)
                .collect(Collectors.toList());
        return wantedFlights;

    }

    public Flight update(Flight flight){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        Flight toReturn = null;
        try {
            transaction = session.beginTransaction();
            session.update(flight);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        finally {
            session.close();
        }
        return toReturn;
    }


    public List<Flight> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List<Flight> flights = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM Flight");
            flights = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return flights;
    }
}
