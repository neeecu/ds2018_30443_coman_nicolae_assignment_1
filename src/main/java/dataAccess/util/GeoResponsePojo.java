package dataAccess.util;

public class GeoResponsePojo {
    private String sunrise;
    private float lng;
    private String countryCode;
    private float gmtOffset;
    private float rawOffset;
    private String sunset;
    private String timezoneId;
    private float dstOffset;
    private String countryName;
    private String time;
    private float lat;

    public String getSunrise() {
        return sunrise;
    }

    public float getLng() {
        return lng;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public float getGmtOffset() {
        return gmtOffset;
    }

    public float getRawOffset() {
        return rawOffset;
    }

    public String getSunset() {
        return sunset;
    }

    public String getTimezoneId() {
        return timezoneId;
    }

    public float getDstOffset() {
        return dstOffset;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getTime() {
        return time;
    }

    public float getLat() {
        return lat;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setGmtOffset(float gmtOffset) {
        this.gmtOffset = gmtOffset;
    }

    public void setRawOffset(float rawOffset) {
        this.rawOffset = rawOffset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public void setTimezoneId(String timezoneId) {
        this.timezoneId = timezoneId;
    }

    public void setDstOffset(float dstOffset) {
        this.dstOffset = dstOffset;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }
}