package dataAccess.entity;
import lombok.*;
import javax.persistence.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue
    private int id;

    @Enumerated(EnumType.STRING)
    private Role userRole;

    private String username;
    private String password;
}