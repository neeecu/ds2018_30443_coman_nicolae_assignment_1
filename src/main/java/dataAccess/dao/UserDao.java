package dataAccess.dao;

import dataAccess.entity.Role;
import dataAccess.entity.User;
import dataAccess.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class UserDao {
    private Session session;

    public int save(User user){
        int userId = -1;
        session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction  = null;
        try {
            transaction = session.beginTransaction();
            if(user.getUserRole()==null){
                user.setUserRole(Role.NOT_DEFINED);
            }
            userId = (Integer) session.save(user);
            user.setId(userId);
            transaction.commit();
        }catch (HibernateException e){
            if(transaction!=null){
                transaction.rollback();
            }
        }
        finally{
            session.close();
            }

        return userId;
    }

    /*
    * returns null when the gievn userId does not exist
    *
    * */
    public User deleteUser(int userId){
        User user = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            user  = findUser(userId);
            if(user!=null) {
                session.delete(user);
                transaction.commit();
            }
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
                //throw exception to notify user that user to delete doesnt exist
                throw e;
            }

        } finally {
            session.close();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public User findUser(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List<User> users = null;
        try {
            transaction = session.beginTransaction();
             Query query = session.createQuery("FROM User WHERE id = :id");
            query.setParameter("id", id);
            users = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }

        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public User findByName(String username) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List<User> users = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username");
            query.setParameter("username", username);
            users = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }

        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List<User> users = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM User");
            users = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return users;
    }

    public User update(User user){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        User toReturn = null;
        try {
            transaction = session.beginTransaction();
            session.update(user);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }

        } finally {
            session.close();
        }
        return toReturn;
    }

}
